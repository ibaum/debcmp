module Debcmp
  class File
    attr_accessor :attrs, :name, :perms, :size, :time, :symlink

    def initialize(name, attrs, perms, size, time, symlink=nil)
      @name = name
      @attrs = attrs
      @perms = perms
      @size = size
      @time = time
      @symlink = symlink
    end
  end

  class DpkgDeb
    attr_accessor :contents, :filename, :name, :version

    def initialize(filename)
      @filename = filename
      @name, @version = `dpkg-deb -W #{filename}`.split
      @contents = parse_contents
    end

    private

    def parse_contents
      parsed = {}
      `dpkg-deb -c #{filename}`.each_line do |line|
        fields = line.split
        parsed[fields[5]] = ::Debcmp::File.new(
          fields[5],
          fields[0],
          fields[1],
          fields[2].to_i,
          fields[3..4],
          fields[7]
        )
      end
      parsed
    end
  end
end
