require 'debcmp/version'
require 'debcmp/dpkg-deb'

module Debcmp
  class Error < StandardError; end
  # Your code goes here...
  class Actions
    class <<self
      def compare(smaller, bigger)
        size_change = 0
        smaller_files = smaller.contents.keys
        bigger_files = bigger.contents.keys
        smaller_has = smaller_files.difference(bigger_files)
        bigger_has = bigger_files.difference(smaller_files)
        puts "smaller has #{smaller_has.length} files that bigger doesn't" unless smaller_has.nil?
        puts "bigger has #{bigger_has.length} files that smaller doesn't" unless bigger_has.nil?

        smaller_has_size = 0
        smaller_has.each { |f| smaller_has_size += smaller.contents[f].size }
        puts "We are removing: #{smaller_has_size} from the package"
        size_change -= smaller_has_size

        bigger_has_size = 0
        bigger_has.each do |f|
          puts "#{f} was added and has size #{bigger.contents[f].size}"
        end
        bigger_has.each { |f| bigger_has_size += bigger.contents[f].size }
        puts "We're adding #{bigger_has_size} to the package"
        size_change += bigger_has_size
        same_file_changes = 0
        (smaller.contents.keys & bigger.contents.keys).each do |f|
          next unless smaller.contents[f].size != bigger.contents[f].size

          difference = bigger.contents[f].size - smaller.contents[f].size
          puts "#{f} has changed by #{difference}" if difference.positive?
          same_file_changes += difference
        end
        puts "Same files have changed by: #{same_file_changes}"
        puts "Package size has changed by: #{size_change + same_file_changes}"
      end
    end
  end
end
